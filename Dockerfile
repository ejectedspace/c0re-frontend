# STAGE 0 - BUILD
FROM node
WORKDIR /opt/new_app
COPY ./ /opt/new_app/

RUN npm install
RUN npm rebuild node-sass
RUN npm run-script build --prod


# STAGE 1 - NGINX
FROM nginx:alpine
WORKDIR /var/www

RUN \
    # Create init file
    echo "#!/bin/sh" >> /init && \
    echo 'sed -i "s|__BACKEND__|${BACKEND_HOST}|" /etc/nginx/nginx.conf' >> /init && \
    echo 'exec nginx' >> /init && \
    chmod a+x /init

CMD ["/init"]
VOLUME ["/data"]

COPY nginx.conf.docker /etc/nginx/nginx.conf
COPY --from=0 /opt/new_app/dist/c0re/ ./