module.exports = {
  // Tailwind Paths
  configJS: './tailwind.js',
  sourceCSS: './src/tailwind.scss',
  outputCSS: './src/styles.scss',
  // PurgeCSS Settings
  purge: false,
  keyframes: false,
  fontFace: false,
  rejected: false,
  whitelist: [],
  whitelistPatterns: [],
  whitelistPatternsChildren: []
}
