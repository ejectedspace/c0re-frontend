import { Component, OnInit } from '@angular/core';
import { ModalService } from './_services/modal.service'
import { ApiService } from './api.service';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  title = 'c0re';
  msg: string
  err_msg: boolean = false
  sug_search: Array<string> = []
  temp_search_query: string
  search_query: string = ''
  username: string
  password: string
  drop_down: boolean = false
  drop_down_pos: object
  avatar: string
  filters = {
    "safe": true,
    "sketchy": true,
    "unsafe": true
  }

  constructor(public api: ApiService, private modal: ModalService, private router: Router, private route: ActivatedRoute) { }

  //login
  ngOnInit(){
    this.api.is_token()
    let filter = window.localStorage.getItem('filter')
    this.avatar = window.localStorage.getItem('avatar')
    if(filter !== null)
      if(filter.indexOf(','))
        this.api.filter = filter.split(',')
      else
        this.api.filter = [filter]
    this.api.filter.map(a => this.filters[a] = false)
  }

  login(event?){
    if(event === undefined || event.keyCode === 13)
      this.api.login(this.username, this.password)
        .subscribe(res => {
          this.api.write_to_storage(res)
          this.api.is_login = true
          this.api.user.username = res['name']
          this.avatar = res['avatarUrl']
          this.modal.close("login_modal")
          this.api.create_token()
          this.password = ''
          this.username = ''
        }, err =>{
          if(err.status === 404 || err.status === 403)
            this.msg = 'wrong username or password', this.err_msg = true
          else
            console.log(err)
        })
  }

  filter(filter: string){
    let pos = this.api.filter.indexOf(filter)
    if(pos > -1){
      this.api.filter.splice(pos, 1)
    } else {
      this.api.filter.push(filter)
      if(this.api.filter.indexOf('') > -1){
        let pos = this.api.filter.indexOf('')
        this.api.filter.splice(pos ,1)
      }
    }
    window.localStorage.setItem('filter', this.api.filter.join(','))
    this.router.navigateByUrl('/test', {skipLocationChange: true}).then(()=>
      this.router.navigate(['/posts']))
  }

  search(event){
    if(this.api.search_query !== '' && this.api.search_query !== this.temp_search_query){
      this.api.getTags(this.api.search_query)
        .subscribe(res => {
          this.sug_search = res['results']
        }, err => {

        })
    }else if(this.api.search_query === ''){
      this.sug_search = []
    }
    if(event.keyCode == 13){
      this.sug_search = []
      this.router.navigate(['/posts', this.api.search_query, ''])
    }
    this.temp_search_query = this.api.search_query
  }

  tag_search(tag){
    this.api.search_query = tag[0]
    this.router.navigate(['/posts', tag[0], ''])
    this.sug_search = []
  }

  open_drop_down(event){
    this.drop_down_pos = event.target.getBoundingClientRect()
    this.drop_down = !this.drop_down
  }

  settings(){
    this.drop_down = false
    this.router.navigate(['/settings', ''])
  }

  // modal
  openModal(id: string){
    this.modal.open(id)
  }
}
