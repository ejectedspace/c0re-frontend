import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { ApiService } from '../api.service'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  allowed_typs = ['jpeg', 'png', 'gif']
  setting: string = 'userdata'
  hover_file: boolean = false
  username: string
  email: string
  password: string
  rep_password: string
  avatar
  avatar_error: boolean = false
  error: boolean = false
  msg: string
  avatar_img: File
  token: string
  tokens: Array<object>
  posts_settings: Object = {'autoplay': true, 'auto_height': true, 'red_box': true, 'loop': true}
  self: boolean
  ver

  constructor(private router: Router, private route: ActivatedRoute, public api: ApiService) {
    this.route.paramMap
      .subscribe(res => {
        let user = res.get('user')
        let _user = window.localStorage.getItem('avatar')
        if(user === null || _user === user){
          this.self = true
          this.avatar = _user
          this.ver = window.localStorage.getItem('ver')
          this.username = window.localStorage.getItem('username')
          this.email = window.localStorage.getItem('email')
          const settings = window.localStorage.getItem('posts_settings')
          if(settings !== null)
            this.posts_settings = JSON.parse(settings)
        }else{
          this.self = false
          this.api.get_user(user)
            .subscribe(res => {
              this.avatar = res['avatarUrl']
              this.username = res['name']
              this.email = res['email']
              this.ver = res['version']
            })
        }
        let set = res.get('setting')
        if(set !== '')
          this.setting = set
        else
          this.setting = 'userdata'
        if(set === 'token')
          this.api.get_user_token(this.username)
            .subscribe(res => {
              this.tokens = res['results']
            })
      })
  }

  ngOnInit() {
  }

  go_setting(set){
    if(this.self)
      this.router.navigate(['/settings', set])
    else
      this.router.navigate(['/settings', this.username, set])
  }

  save() {
    this.error = false
    if(this.password === this.rep_password){
      let data = {"version": this.ver}
      let old_username
      if(this.self)
        old_username = window.localStorage.getItem('username')
      else
        this.route.paramMap.subscribe(res =>  old_username = res.get('user'))
      if(old_username !== this.username)
        data['name'] = this.username
      if(this.avatar_img){
        data['avatarToken'] = this.token
        data['avatarStyle'] = 'manual'
      }
      data['email'] = this.email
      if(this.password)
        data['password'] = this.password
      this.api.update_user(old_username, data)
    } else {
      this.msg = "passwords don't match"
      this.error = true
    }
  }

  upload() {
    return this.api.temp_upload(this.avatar_img)
      .subscribe(res => {
        this.token = res["token"]
      })
  }

  file_drop(file: FileList) {
    this.avatar_error = false
    if(file.length === 1) {
      let _file = file[0]
      if(this.allowed_typs.indexOf(_file.type.split('/')[1]) > -1){
        this.getBase64(_file).then(
          data => this.avatar = data)
        this.avatar_img = _file
        this.upload()
      }else{
        this.msg = 'wrong file type'
        this.avatar_error = true
      }
    }else if(file.length > 1){
      this.avatar_error = true
      this.msg = 'Only 1 file'
    }
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    });
  }

  delete_token(token: string){
    let user = window.localStorage.getItem('username')
    this.api.delete_user_token(user, token)
      .subscribe(res => {
        if(token === window.localStorage.getItem('token'))
          this.api.logout()
        else
          this.api.get_user_token(user)
            .subscribe(res => {
              this.tokens = res['results']
            })
      })
  }

  posts_change() {
    window.localStorage.setItem('posts_settings', JSON.stringify(this.posts_settings))
  }

  calc_time(date){
    let time = new Date(date).valueOf()
    let now_time = new Date().valueOf()
    time = (now_time - time) / 1000
    let time_msg: string
    if(time < 60)
      time_msg = 'a few seconds ago'
    else if(time < 3600)
      if(Math.trunc(time / 60) === 1)
        time_msg = 'a minute ago'
      else
        time_msg = Math.trunc(time / 60) + ' minutes ago'
    else if(time < 86400)
      if(Math.trunc(time / 3600) === 1)
        time_msg = 'a hour ago'
      else
        time_msg = Math.trunc(time / 3600) + ' hours ago'
    else if(time < 2592000)
      if(Math.trunc(time / 86400) === 1)
        time_msg = 'a day ago'
      else
        time_msg = Math.trunc(time / 86400) + ' days ago'
    else if(time < 31536000)
      if(Math.trunc(time / 2678400) === 1)
        time_msg = 'a month ago'
      else
        time_msg = Math.trunc(time / 2678400) + ' months ago'
    else
      if(Math.trunc(time / 31536000) === 1)
        time_msg = 'a year ago'
      else
        time_msg = Math.trunc(time / 31536000) + ' years ago'
    return time_msg
  }

}
