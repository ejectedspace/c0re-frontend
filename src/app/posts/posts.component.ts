import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HostListener } from "@angular/core";
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  total_items: number = 0
  search
  items: Array<Object> = []
  open_item: Object
  open_data: Object
  rows
  open_id
  col
  ids_rows: Object = {}
  opened_posts: Object = {}
  post_edit: boolean = false
  new_comment: string
  new_tag: String = ''
  new_tags: Array<String> = []
  max_height: number
  sug_tag: Array<string> = []
  max_id: number
  del_img: boolean = false
  option: object = {'autoplay': true, 'auto_height': true, 'red_box': true, 'loop': true}
  filter

  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) {
    this.onResize()
    this.route.paramMap
      .subscribe(res => {
        if(res.get('query') != this.api.search_query || this.search !== this.api.search_query){
          this.api.search_query = res.get('query')
          this.search = this.api.search_query
          this.ngOnInit()
        }
        if(res.get('query') === ''){
          this.router.navigate(['/posts', ''])
          this.ngOnInit()
        }
        this.open_id =+ res.get('id')
        if(this.open_id.toString() !== 'NaN'){
          if(this.open_id !== 0 && Object.keys(this.opened_posts).indexOf(this.open_id.toString()) === -1)
            this.api.getPost(this.open_id)
              .subscribe(res => {
                this.opened_posts[this.open_id] = res
                this.open_data = res
              })
          else if(this.open_id !== 0)
            this.open_data = this.opened_posts[this.open_id]
        } else {
          this.router.navigate(['/posts', res.get('id'), ''])
        }
      })
   }

  @HostListener('window:resize', ['$event'])
    onResize(event?) {
      this.max_height = window.innerHeight - 200
      let width = window.innerWidth;
      let _col
      if(width >= 1200) {
        _col = 10
      }else if(width >= 992) {
        _col = 8
      }else if(width >= 768) {
        _col = 6
      }else{
        _col = 4
      }
      if(_col !== this.col){
        this.col = _col
        this.calc_rows()
      }
    }

  ngOnInit() {
    if(this.open_id)
      this.max_id = this.open_id + 30
    this.api.getPosts(null, this.max_id)
      .subscribe(res => {
        this.items = res['results']
        this.total_items = res['total']
        this.calc_rows()
      }, err => {
        if(err.status !== 504 && err.status !== 404 && err.status !== 400)
          window.location.reload()
      })
    const setting = window.localStorage.getItem('posts_settings')
    if(setting !== null)
      this.option = JSON.parse(setting)
  }

  open_row(row) {
    if(this.ids_rows[this.open_id] === row) {
      this.open_post()
      return true
    } else {
      return false
    }
  }

  open_post(){
    for(var i = 0; i < this.items.length; i++)
      if(this.items[i]["id"] === this.open_id)
        this.open_item = this.items[i]
  }

  calc_rows() {
    if(this.items !== undefined) {
      let items_count = this.items.length
      let _rows = Math.ceil(items_count / this.col)
      this.rows = Array.apply(null, {length: _rows}).map(Number.call, Number)
    }
  }

  row_split(row) {
    let skip_item = row * this.col - 1
    let out = []
    for(var i = 0; this.items.length > i; i++){
      let item = this.items[i]
      let x = this.items.indexOf(item)
      if(x > skip_item &&  x < skip_item + this.col + 1) {
        out.push(item)
        this.ids_rows[item["id"]] = row
      }
    }
    if(out.length !== this.col){
      while(out.length < this.col){
        out.push({'id': null})
      }
    }
    return out
  }

  onScroll(){
    if(this.total_items > this.items.length){
      let offset = this.items.length
      this.api.getPosts(offset, this.max_id)
        .subscribe(res => {
          res['results'].map(a => this.items.push(a))
          this.calc_rows()
        })
    }
  }

  close_post() {
    this.router.navigate(['/posts', ''])
  }

  save_post(save: boolean) {
    if(save){
      if(!this.del_img){
        let tags = this.open_data['tags'].map(a => a.names[0])
        this.new_tags.map(a => tags.push(a))
        let safty = null
        if(this.filter !== this.open_data['safety'])
          safty = this.filter
        this.api.update_post(this.open_id, this.open_data['version'], tags, safty)
          .subscribe(res => {
            this.open_data = res
            this.opened_posts[this.open_id] = res
            this.new_tags = []
          }, err => {
            this.new_tags = []
          })
      }else{
        this.api.del_post(this.open_id, this.open_data['version'])
          .subscribe(res => {
            let id = this.open_id
            let pos_items = this.items.indexOf(this.open_item)
            this.router.navigate(['/posts', ''])
            delete this.open_data[id]
            this.items.splice(pos_items, 1)
          })
      }
      this.new_tag = ''
      this.post_edit = false
      this.del_img = false
      this.filter = ''
    }else{
      this.filter = ''
      this.new_tag = ''
      this.del_img = false
      this.new_tags = []
      this.post_edit = false
    }
  }

  add_score(value){
    let x
    if(this.open_data['ownScore'] === value) {
      this.open_data['ownScore'] = 0
      x = 0
    } else if(this.open_data['ownScore'] !== 0 && value !== this.open_data['ownScore']) {
      x = value
      this.open_data['ownScore'] = x
    } else {
      this.open_data['ownScore'] = value
      x = value
    }
    this.api.change_score(this.open_id, x)
    .subscribe(res => {
      this.open_data = res
      this.opened_posts[this.open_id] = res
      })
  }

  fav(){
    if(this.open_data['ownFavorite']){
      this.open_data['ownFavorite'] = false
      this.api.del_fav(this.open_id)
        .subscribe(res => {
          this.open_data = res
          this.opened_posts[this.open_id] = res
        })
    } else {
      this.open_data['ownFavorite'] = true
      this.api.add_fav(this.open_id)
        .subscribe(res => {
          this.open_data = res
          this.opened_posts[this.open_id] = res
        })
    }
  }

  del_tag(tag){
    let pos = this.open_data['tags'].map(function(x) {return x.names[0]}).indexOf(tag[0])
    this.open_data['tags'].splice(pos, 1)
  }

  add_tag(event) {
    if(this.new_tag !== ''){
      this.api.getTags(this.new_tag)
        .subscribe(res => {
          this.sug_tag = res['results'].filter(a => {if(this.new_tags.indexOf(a.names[0]) == -1) return a})
        })
    }else{
      this.sug_tag = []
    }
    if(event.keyCode == 13){
      if(this.new_tags.indexOf(this.new_tag) == -1 && this.new_tag !== '')
        this.new_tags.push(this.new_tag.replace(/ /g, '_').replace(/:/g, '_'))
      this.new_tag = ''
    }
  }

  add_sug_tag(tag) {
    this.new_tags.push(tag[0])
    this.sug_tag = []
    this.new_tag = ''
  }

  del_new_tag(tag){
    let pos = this.new_tags.indexOf(tag)
    this.new_tags.splice(pos, 1)
  }

  add_comment(){
    if(this.new_comment !== undefined || this.new_comment !== ''){
      this.api.create_comment(this.open_id, this.new_comment)
        .subscribe(res => {
          this.new_comment = ''
          this.open_data['comments'].push(res)
        }, err => {
          console.log(err)
        })
    }
  }

  add_score_comment(value: number, id: number) {
    let x
    let comm_pos = this.open_data['comments'].map(function(x) {return x.id}).indexOf(id)
    let comment = this.open_data['comments'][comm_pos]
    if(comment['ownScore'] === value) {
      comment['ownScore'] = 0
      x = 0
    } else if(comment['ownScore'] !== 0 && value !== comment['ownScore']) {
      x = value
      comment['ownScore'] = x
    } else {
      comment['ownScore'] = value
      x = value
    }
    this.api.change_score_comment(id, x)
      .subscribe(res => {
        this.open_data['comments'][comm_pos] = res
        this.opened_posts[this.open_id] = res
      })
  }

  calc_time(date){
    let time = new Date(date).valueOf()
    let now_time = new Date().valueOf()
    time = (now_time - time) / 1000
    let time_msg: string
    if(time < 60)
      time_msg = 'a few seconds ago'
    else if(time < 3600)
      if(Math.trunc(time / 60) === 1)
        time_msg = 'a minute ago'
      else
        time_msg = Math.trunc(time / 60) + ' minutes ago'
    else if(time < 86400)
      if(Math.trunc(time / 3600) === 1)
        time_msg = 'a hour ago'
      else
        time_msg = Math.trunc(time / 3600) + ' hours ago'
    else if(time < 2592000)
      if(Math.trunc(time / 86400) === 1)
        time_msg = 'a day ago'
      else
        time_msg = Math.trunc(time / 86400) + ' days ago'
    else if(time < 31536000)
      if(Math.trunc(time / 2678400) === 1)
        time_msg = 'a month ago'
      else
        time_msg = Math.trunc(time / 2678400) + ' months ago'
    else
      if(Math.trunc(time / 31536000) === 1)
        time_msg = 'a year ago'
      else
        time_msg = Math.trunc(time / 31536000) + ' years ago'
    return time_msg
  }

  get_url(img) {
    let url = window.location.origin + '/' + img
    return encodeURIComponent(url)
  }
}
