import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'matchUrl'
})
export class MatchUrlPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(/\b(https?:\/\/(?:www\.)?|www\.)([^\s()<>]+(?:\([\w\d]+\)|([^,\.\(\)<>!?\s]|\/)))/g, function(url, httpwww, hostandpath) {
      if (httpwww === 'www.') {
          url = 'http://' + url;
      }
      url = url.replace(/@/g, '&#64;');
      hostandpath = hostandpath.replace(/@/g, '&#64;');
      if(hostandpath.length > 30) {
        hostandpath = hostandpath.substring(0,29)+"...";
      }
      var path = null;
      return '<a href="' + url + '" target="_blank" class="no-underline text-purple-light hover:text-purple" rel="noopener noreferrer">' + hostandpath + '</a>';
    });
  }

}
