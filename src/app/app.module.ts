//angular stuff
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//Infinite Scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

//font awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';

//services
import { ApiService } from './api.service';
import { ModalService } from './_services/modal.service'

//components
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UploadComponent } from './upload/upload.component';
import { FileDropDirective } from './file-drop.directive';
import { UserComponent } from './user/user.component';
import { SettingsComponent } from './settings/settings.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MatchUrlPipe } from './match-url.pipe';
import { ModalDirective } from './_directives/modal.directive';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    NotFoundComponent,
    UploadComponent,
    FileDropDirective,
    UserComponent,
    SettingsComponent,
    MatchUrlPipe,
    ModalDirective
  ],
  imports: [
    //font awesome
    AngularFontAwesomeModule,
    //Angular stuff
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    InfiniteScrollModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [ApiService, ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
