import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http"
import { map } from "rxjs/operators"
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  api: string = '/api/'
  is_login: boolean = false
  search_query
  header = {}
  filter = ["sketchy", "unsafe"]
  user = {
    "username":'',
    "admin": false,
    "mod": false
  }

  constructor(private _http: HttpClient, private router: Router) { }

  get(url){
    return this._http.get(this.api + url, {headers : this.header})
      .pipe(map(res => res))
  }

  post(url, data){
    return this._http.post(this.api + url, data=data, {headers : this.header})
      .pipe(map(res => res))
  }

  put(url, data){
    return this._http.put(this.api + url, data=data, {headers : this.header})
  }

  delete(url, data?){
    return this._http.delete(this.api + url, {headers : this.header})
      .pipe(map(res => res))
  }
  
  // post/s handler 
  getPosts(offset?: number, max_id?: number) {
    let query = ''
    if(this.filter.length !== 0 && this.filter[0] !== "")
      query = '-rating:' + this.filter.join(',') + ' '
    if(max_id)
      query = query + 'id-max:' + max_id.toString() + ' '
    if(this.search_query)
      query = query + this.search_query
    if(offset)
      query = query + '&offset=' + offset
    return this.get('posts/?query=' + query)
  }

  del_post(id: number, version: number) {
    return this.delete('post/' + id + '?version=' + version)
  }

  getPost(id) {
    return this.get('post/' + id.toString())
  }

  reverse_search(token) {
    let data = {'contentToken': token}
    return this.post('posts/reverse-search', data)
  }

  change_score(id: number, score: number) {
    let data = {"score": score}
    return this.put('post/' + id + '/score', data)
  }

  add_fav(id: number){
    return this.post('post/' + id + '/favorite', {})
  }

  del_fav(id: number){
    return this.delete('post/' + id + '/favorite')
  }

  update_post(id: number, version: number, tags: Array<String>, safety ?: string){
    let data = {"tags": tags, "version": version}
    if(safety)
      data['safety'] = safety
    return this.put('post/' + id, data)
  }

  create_comment(post_id: number, comment: string) {
    let data = {"postId": post_id, "text": comment}
    return this.post('comments', data)
  }

  change_score_comment(id: number, score: number) {
    let data = {"score": score}
    return this.put('comment/' + id + '/score', data)
  }
  // upload
  temp_upload(img){
    const data: FormData = new FormData()
    data.append('content', img)
    return this.post('uploads', data)
  }

  upload(token: string, filter: string, tags: Array<String>){
    let data = {'contentToken': token, 'safety': filter, 'tags': tags}
    return this.post('posts', data)
  }

  // tags handler
  getTags(query){
    query = '?query=' + query + '* sort:usages&limit=15&fields=names,category,usages'
    let request = 'tags/' + query
    return this.get(request)
  }

  // comments
  get_comments(query?: string, offset?: number, limit?: number){
    let request = '?query='
    if(query)
      request = request + query
    if(offset)
      request = request + '&offset=' + offset
    if(limit)
      request = request + '&limit=' + limit
    return this.get('comments/' + request)
  }

  // user
  get_user(user: string){
    return this.get('user/' + user)
  }

  update_user(user: string, data: object){
    this.put('user/' + user, data)
      .subscribe(res => {
        if(user === this.user.username)
          this.write_to_storage(res)
      })
  }

  get_user_token(user: string){
    return this.get('user-tokens/' + user)
  }

  delete_user_token(user: string, token: string) {
    return this.delete('user-token/' + user + '/' + token)
  }

  //login-token handler
  is_token(){
    if(window.localStorage.getItem('token'))
      this.login_token()
  }

  login(username, password){
    this.header = new HttpHeaders().set("Authorization", "Basic " + btoa(username + ":" + password))
    let request = 'user/' + username +'?bump-login=true'
    return this.get(request)
  }

  login_token(){
    let token = window.localStorage.getItem('token')
    let username = window.localStorage.getItem('username')
    this.header = new HttpHeaders().set("Authorization", "Token "+ btoa(username + ':' + token))
    let request = 'user/' + username +'?bump-login=true'
    this.get(request)
      .subscribe(res => {
        this.write_to_storage(res)
        this.is_login = true
        this.user.username = res['name']
      }, err => {
        this.header = {}
        this.is_login = false
        window.localStorage.clear()
      })
  }

  create_token(){
    let userTokenRequest = {
      enabled: true,
      note: 'Web Login Token'
    };
    let username = window.localStorage.getItem('username')
    let request = 'user-token/' +  username
    this.post(request, userTokenRequest)
      .subscribe(res => {
        let token = res['token']
        window.localStorage.setItem('token', token)
        this.header = new HttpHeaders().set("Authorization", "Token "+ btoa(username + ':' + token))
      })
  }

  delete_token(){
    let username = window.localStorage.getItem('username')
    let token = window.localStorage.getItem('token')
    let request = 'user-token/' + username + '/' + token
    this.delete(request)
      .subscribe(res => {
        window.localStorage.clear()
      })
  }

  write_to_storage(res){
    window.localStorage.setItem('username', res['name'])
    if(res['rank'] === 'administrator')
      window.localStorage.setItem('admin', 'true'), this.user.admin = true
    if(res['rank'] === 'moderator')
      window.localStorage.setItem('mod', 'true'), this.user.mod = true
    window.localStorage.setItem('rank', res['rank'])
    window.localStorage.setItem('avatar', res['avatarUrl'])
    if(res['email'] !== null)
      window.localStorage.setItem('email', res['email'])
    window.localStorage.setItem('ver', res['version'])
  }

  logout(){
    this.delete_token()
    this.header = {}
    this.is_login = false
  }
}
