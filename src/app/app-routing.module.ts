import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component'
import { NotFoundComponent } from './not-found/not-found.component';
import { UploadComponent } from './upload/upload.component';
import { UserComponent } from './user/user.component'
import { SettingsComponent } from './settings/settings.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/posts/',
    pathMatch: 'full'
  },
  {
    path: 'posts/:id',
    component: PostsComponent
  },
  {
    path: 'posts/:query/:id',
    component: PostsComponent
  },
  {
    path: 'posts',
    redirectTo: '/posts/',
    pathMatch: 'full'
  },
  {
    path: 'upload',
    component: UploadComponent
  },
  {
    path: 'user/:user',
    component: UserComponent
  },
  {
    path: 'settings/:setting',
    component: SettingsComponent
  },
  {
    path: 'settings/:user/:setting',
    component: SettingsComponent
  },
  {
    path: 'settings',
    redirectTo: '/settings/',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
