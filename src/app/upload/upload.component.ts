import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  hover_file: boolean = false
  allowed_typs = ['jpeg', 'png', 'gif', 'webm', 'mp4', 'swf']
  error: boolean = false
  upload_warning: boolean = false
  upload_error: boolean = false
  upload_msg: Object = {}
  msg: string
  file
  file_base64
  file_type
  tag: string
  tags: Array<String> = []
  sug_tags: Array<String> = []
  filter: string = 'safe'
  no_repost = false
  is_upload = false

  constructor(public api: ApiService, private router: Router) { }

  ngOnInit() {
  }

  dropzone(event: boolean) {
    this.hover_file = event
  }

  file_Drop(file: FileList) {
    this.no_repost = false
    this.upload_warning = false
    this.upload_error = false
    this.error = false
    this.tags = []
    this.tag = ''
    if(file.length === 1) {
      let _file = file[0]
      if(this.allowed_typs.indexOf(_file.type.split('/')[1]) > -1){
        this.file_type = _file.type.split('/')[0]
        this.getBase64(_file).then(
          data => this.file_base64 = data)
        this.file = _file
      }else{
        this.msg = 'wrong file type'
        this.error = true
      }
    }else if(file.length > 1){
      this.error = true
      this.msg = 'Only 1 file'
    }
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => resolve(reader.result)
      reader.onerror = error => reject(error)
    });
  }

  add_tag(event){
    if(this.tag !== ''){
      this.api.getTags(this.tag)
        .subscribe(res => {
          this.sug_tags = res['results'].filter(a => {if(this.tags.indexOf(a.names[0]) == -1) return a})
        })
    }else{
      this.sug_tags = []
    }
    if(event.keyCode == 13){
      if(this.tags.indexOf(this.tag) == -1 && this.tag !== '')
        this.tags.push(this.tag.replace(/ /g, '_').replace(/:/g, '_'))
      this.tag = ''
    }
  }

  add_sug_tag(tag) {
    this.tags.push(tag[0])
    this.sug_tags = []
    this.tag = ''
  }

  del_tag(tag){
    let _tag = this.tags.indexOf(tag)
    this.tags.splice(_tag, 1)
  }

  upload(){
    this.is_upload = true
    this.api.temp_upload(this.file)
      .subscribe(res => {
        let token = res['token']
        this.api.reverse_search(token)
          .subscribe(res => {
            if(res['exactPost'] === null && (res['similarPosts'].length === 0 || this.no_repost)){
              this.api.upload(token, this.filter, this.tags)
                .subscribe(res => {
                  this.router.navigate(['posts', res['id']])
                })
              } else if(res['exactPost'] !== null) {
                this.is_upload = false
                this.upload_msg['msg'] = 'post already exists'
                this.upload_msg['id'] = res['exactPost']['id']
                this.upload_error = true
              } else {
                this.is_upload = false
                let msg
                if(res['similarPosts'].length === 1)
                  msg = 'a similar post was found, '
                else
                  msg = 'some similar posts were found, '
                this.upload_msg['msg'] = msg + 'if your sure it`s not a repost, press upload again'
                this.upload_msg['items'] = []
                res['similarPosts'].map(a => this.upload_msg['items'].push({'id': a.post.id, 'thumbnailUrl': a.post.thumbnailUrl}))
                this.upload_warning = true
                this.no_repost = true
              }
          })
      })
  }
}

