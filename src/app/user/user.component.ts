import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service'
import { ActivatedRoute, Router } from '@angular/router'
import { ModalService } from '../_services/modal.service'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: string
  username: string
  rank: string
  avatar: string
  posts: Array<Object>
  favs: Array<Object>
  comments: Array<Object>
  comments_thumb: Object = {}

  constructor(public api: ApiService, private route: ActivatedRoute, private modal: ModalService, private router: Router) { 
    this.route.paramMap
      .subscribe(res => {
        this.user = res.get('user')
        this.api.get_user(this.user)
          .subscribe(res => {
            this.rank = res['rank']
            this.avatar = res['avatarUrl']
            this.username = res['name']
            if(res['uploadedPostCount'] > 0)
              this.get_posts()
            if(res['favoritePostCount'] > 0)
              this.get_favs()
            if(res['commentCount'] > 0)
              this.get_comments()
            this.api.search_query = ''
          })
        })
   }

  ngOnInit() { }

  toSettings(){
    this.router.navigate(['/settings', this.username, ''])
  }

  openModal(id: string){
    this.modal.open(id)
  }

  get_posts(){
    this.api.search_query = 'submit:' + this.username + '&limit=20'
    this.api.getPosts()
      .subscribe(res => {
        this.posts = res['results']
      })
  }

  get_favs(){
    this.api.search_query = 'fav:' + this.username + '&limit=20'
    this.api.getPosts()
      .subscribe(res => {
        this.favs = res['results']
      })
  }

  get_comments(){
    this.api.get_comments('user:' + this.username, null, 20)
      .subscribe(res => {
        this.comments = res['results']
      })
    this.api.search_query = 'comment:' + this.username + '&limit=20&fields=thumbnailUrl,id'
    this.api.getPosts()
      .subscribe(res => {
        res['results'].map(a => this.comments_thumb[a.id] = a.thumbnailUrl)
      })
  }
}
